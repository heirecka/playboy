#! /bin/bash

MIN_DELAY_PER_HOST=5

export LC_ALL=C
timestamps_file=${PALUDIS_HOOKS_TMPDIR:-${ROOT}/var/tmp/paludis}/rate_limiter.${PALUDIS_PID}

hook_auto_names() {
    echo sync_all_pre sync_pre sync_post sync_fail sync_all_post
}

hook_run_sync_all_pre() {
    : >"${timestamps_file}"
}

hook_run_sync_all_post() {
    rm -f "${timestamps_file}"
}

hook_run_sync_fail() {
    hook_run_sync_post
}

myhost() {
    # XXX wrong if repo_name changes across the sync, but shouldn't
    # happen often enough to matter
    local host=$(${CAVE} print-repository-metadata --raw-name sync --format '%v\n' ${TARGET})
    host=${host#*://}
    host=${host%%/*}
    host=${host#*@}
    echo ${host}
}

escape() {
    sed -e 's/[\^.[$()|*+?{\\]/\\&/g' <<<"${1}"
}

hook_run_sync_pre() {
    local myhost=$(myhost)
    if [[ -n ${myhost} ]]; then
        local lasttime=$(grep -E "^$(escape ${myhost}) [0-9]+\$" "${timestamps_file}" | cut -d' ' -f2)
        local delay=$(( ${lasttime:-0} + ${MIN_DELAY_PER_HOST} - $(date +%s) ))
        if [[ ${delay} -gt 0 ]]; then
            source "${PALUDIS_EBUILD_DIR}/echo_functions.bash"
            einfo "Sleeping for ${delay} second(s) to avoid flooding ${myhost}..."
            sleep ${delay}
        fi
    fi
    return 0
}

hook_run_sync_post() {
    local myhost=$(myhost)
    if [[ -n ${myhost} ]]; then
        local grepv=$(grep -E -v "^$(escape ${myhost}) [0-9]+\$" "${timestamps_file}")
        echo "${myhost} $(date +%s)" >"${timestamps_file}"
        [[ -n ${grepv} ]] && echo "${grepv}" >>"${timestamps_file}"
    fi
    return 0
}


#!/usr/bin/env bash

export PALUDIS_NO_WRITE_CACHE_CLEAN=yes

PLAYBOY_DIR="${CI_PROJECT_DIR}"
export PALUDIS_LOG_DIRECTORY="/root/paludis-logs"
export PALUDIS_HOOKS_TMPDIR="/root/paludis-hooks-tmp"

die() {
    local ret line
    ret=${1}
    shift
    for line in "$@"; do
        echo "${line}"
    done >&2
    exit ${ret}
}

# exherbo,gentoo?
if [[ -n ${1} ]]; then
    TASK="${1}"
else
    die 1 "No task given."
fi
if [[ -n ${2} ]]; then
    ALWAYS_SENDMAIL=yesplease
fi

run() {
    local my_dir="${PLAYBOY_DIR}/${TASK}"
    local lockfile="${my_dir}/lockfile"
    {
        if ! flock --exclusive --nonblock 5; then
            echo -e "\nFailed to lock ${lockfile}, is another instance already running?\n"
            return 1
        fi

        local output_dir="${my_dir}/output"
        rm -f "${output_dir}"/*
        local TARBALL PLAYBOY_OPTIONS
        source "${my_dir}/task_settings.bash"

        ruby "${PLAYBOY_DIR}/playboy.rb" --environment ${ENVIRONMENT} --log-level warning \
            "${PLAYBOY_OPTIONS[@]}" --output-dir "${output_dir}" &&
        ruby "${PLAYBOY_DIR}/cleanup.rb" --environment ${ENVIRONMENT} --log-level warning &&
        (
            cd "${output_dir}" &&
            echo -e "\nTarring up things: tar cvjf ../${TARBALL} *.repository"
            tar cvjf ../${TARBALL} *.repository
            s3cmd --host-bucket cellar-c2.services.clever-cloud.com --host cellar-c2.services.clever-cloud.com --access_key ${CELLAR_ACCESS_KEY} --secret_key ${CELLAR_SECRET_KEY} put -P ../${TARBALL} s3://unavailable.exherbolinux.org/
        ) &&
        echo -e "\nGeneration complete.\n" || return 1
    } 5>"${lockfile}"
}

main() {
    local ENVIRONMENT date0 date1 run_date result
    date0=$(date -u +%_D) || die 1 "date0 failed"
    date1=$(date -u +%T) || die 1 "date1 failed"

    chown -R paludisbuild:paludisbuild "${PLAYBOY_DIR}"
    mkdir -p /root/paludis-logs/{,paludis}
    chown -R paludisbuild:paludisbuild /root/paludis-logs
    mkdir -p /root/paludis-hooks-tmp
    chown -R paludisbuild:paludisbuild /root/paludis-hooks-tmp

    run_date=$(date -u +%Y-%m-%d_%H:%M)
    run && result=Success || result=Failure

    if [[ ${result} == Failure && -n ${ENVIRONMENT} ]]; then
        {
            echo -e "\n# cave --environment ${ENVIRONMENT} info"
            cave --environment ${ENVIRONMENT} info
        }
    fi
}

main

# vim: set sw=4 ts=4 tw=100 et :
